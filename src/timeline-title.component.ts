import { Component, OnInit } from '@angular/core';
import { GlobalVariable } from 'assets/globals';
import { SlimScrollOptions } from 'ng2-slimscroll';

@Component({
  selector: 'app-timeline-title',
   template: `
<div class="card">
  <div style=" position: relative;">
    <div style="margin: 0 37px 5px;overflow: hidden;" >
      <div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; ">
        <div style=" height: 350px;display: block;" >
          <div style="padding-right: 0px;background: white;margin: 0; height: 100%;overflow-y: auto;   padding: 0px 0; border-left: none; border-right: none;border-color: #ddd; font-size: 13px;"  slimScroll [options]="options">
            <div *ngFor="let data of itemsDay1" >
              <span style="position: relative; display:block;padding:0.75rem 1.25rem;background-color:#fff;border:1px solid #ddd;">
                    <img src="{{data.image}}" alt="{{data.screen_name}}" style=" width: 48px; height: 48px;  vertical-align: middle;" onerror="this.style.display='none'">
                    <strong class="">{{data.name}}</strong>
                     <span style="color: #666;">@{{data.screen_name}}</span>
                  <a href="{{data.post_link}}" target="_blank" style="color: #666; white-space: nowrap;font-size: .9em;">{{data.timestamp }} - {{data.type}}</a>
                     <a href="{{data.post_link}}" target="_blank">
                    <p [innerHTML]="data.tweet_text"></p>
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; ">
        <div style=" height: 350px;display: block;" >
          <div style="padding-right: 0px;background: white;margin: 0; height: 100%;overflow-y: auto;   padding: 0px 0; border-left: none; border-right: none;border-color: #ddd;
                font-size: 13px;"  slimScroll [options]="options">
            <div *ngFor="let data of itemsDay2">
              <span style="position: relative; display:block;padding:0.75rem 1.25rem;background-color:#fff;border:1px solid #ddd;">
                    <img src="{{data.image}}" alt="{{data.screen_name}}" style=" width: 48px; height: 48px;  vertical-align: middle;"  onerror="this.style.display='none'">
                    <strong class="">{{data.name}}</strong>
                     <span style="color: #666;">@{{data.screen_name}}</span>
                  <a href="{{data.post_link}}" target="_blank"  style="color: #666; white-space: nowrap;font-size: .9em;">{{data.timestamp }} - {{data.type}}</a>
                     <a href="{{data.post_link}}" target="_blank">
                    <p [innerHTML]="data.tweet_text"></p>
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>

      <div style=" float: left; width: 33.33333%; min-height: 1px;  padding-right: 3px;  padding-left: 3px; " >
        <div style=" height: 350px;display: block;"  >
          <div style="padding-right: 0px;background: white;margin: 0; height: 100%;overflow-y: auto;   padding: 0px 0; border-left: none; border-right: none;border-color: #ddd;
                font-size: 13px;"  slimScroll [options]="options">
            <div *ngFor="let data of itemsDay3" >
              <span style="position: relative; display:block;padding:0.75rem 1.25rem;background-color:#fff;border:1px solid #ddd;">
                    <img src="{{data.image}}" alt="{{data.screen_name}}" style=" width: 48px; height: 48px;  vertical-align: middle;" onerror="this.style.display='none'">
                    <strong class="">{{data.name}}</strong>
                     <span style="color: #666;">@{{data.screen_name}}</span>
                  <a href="{{data.post_link}}" target="_blank" style="color: #666; white-space: nowrap;font-size: .9em;">{{data.timestamp }} - {{data.type}}</a>
                     <a href="{{data.post_link}}" target="_blank">
                    <p [innerHTML]="data.tweet_text"></p>
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>


    </div>
    <div style="margin: 0 37px;">
      <!--<div class="row-timeline-page">-->
      <div style="display: table;table-layout: fixed;width: 100%; border-left: 1px solid #515151; border-right: 1px solid #515151;">
        <div style="border-left: 1px solid #515151;border-right: 1px solid #515151;display: table-cell;font-weight: bold;line-height: 30px; padding: 0 10px;vertical-align: middle; float: left; width: 33.33333%;" class=" text-primary"  *ngFor="let date of items; let ind = index">{{date.text}}</div>
        <!--<div class="cell-timeline-page text-primary col-md-4" *ngFor="let date of items; let ind = index">{{date.text}}</div>-->
      </div>
    </div>
    <div style="left: 0; position: absolute;bottom: 0;">
      <span style="cursor:pointer" id="prev-timeday" (click)="prevDay($event)">
            <img height="27%" width="25%" style="float: left" src = 'assets/img/left.png' > 
        </span>
    </div>
    <div style="right: 0; position: absolute;bottom: 0;">
      <span style="cursor:pointer" id="next-timeday" (click)="nextDay($event)">
            <img height="27%" width="25%" style="float: right" src = 'assets/img/right.png' > 
      </span>
    </div>
  </div>
</div>`,
  styles: ['  html {box-sizing: border-box; }*, *::before, *::after {  box-sizing: inherit; } body { background-color: #e4e5e6; } a {color: #20a8d8; text-decoration: none; } a:focus, a:hover {color: #167495; text-decoration: underline; } a:focus {outline: 5px auto -webkit-focus-ring-color; outline-offset: -2px; } .card { position: relative; display: block; margin-bottom: 0.75rem; background-color: #e4e5e6;  border: 1px solid #e4e5e6; }']

})
export class BaseWidgetSocmedTimelineComponent implements OnInit {

  options: SlimScrollOptions;
  private dates:any;
  private date: any;
  private prevDate: any;
  private nextDate: any;
  public date1: any;
  public date2: any;
  public date3: any;
  private days:any;
  private items: any;

  private itemsDay1: any;
  private itemsDay2: any;
  private itemsDay3: any;

  private splitDays1: any;
  private splitDays2: any;
  private splitDays3: any;

  private tgl1 ="20170423";
  private tgl2="20170424";
  private tgl3 ="20170425";

  private hari1:any;
  private hari2:any;
  private hari3:any;

  public timeline = {
       "20170423": {
         "items1":[
                   {
                  "tweet_id": "856167623356104705",
                  "created_date": "Sun Apr 23 22:27:22 WIB 2017",
                  "user_id": "45843545",
                  "name": "\\w/,",
                  "screen_name": "WawanPapAiko",
                  "image": "http://pbs.twimg.com/profile_images/855642238474371072/6qFK4DH8_normal.jpg",
                  "tweet_text": "@kompascom @yunartowijaya Apa cuma gue yg merasa @jokowi 🙈🙉🙊 tunggu waktu atau pura2 tidak tahu..?\nPercuma infrastu… https://t.co/0qnRUaq2GC",
                  "sentiment": "0",
                  "timestamp": 1492961242000
                  },
                  {
                  "tweet_id": "856167043489345536",
                  "created_date": "Sun Apr 23 22:25:04 WIB 2017",
                  "user_id": "316313045",
                  "name": "banyubenning",
                  "screen_name": "adoncrush",
                  "image": "http://pbs.twimg.com/profile_images/832207924210475008/5Bws2d8K_normal.jpg",
                  "tweet_text": "RT @kompascom: Khofifah: Kelompok Anti-Pancasila Bergerak Melalui Forum Kajian Agama. https://t.co/Wn66iPvFJd",
                  "sentiment": "0",
                  "timestamp": 1492961104000
                  },
                  {
                  "tweet_id": "856097899419025408",
                  "created_date": "Sun Apr 23 17:50:19 WIB 2017",
                  "user_id": "781521757559672833",
                  "name": "Iwel 69",
                  "screen_name": "IwelSanjaya",
                  "image": "http://pbs.twimg.com/profile_images/854310009353535488/h25Jm8y6_normal.jpg",
                  "tweet_text": "RT @kompascom: Sandiaga: Gaji Saya sebagai Wagub Akan Disumbangkan ke Kaum Dhuafa. https://t.co/KTTL06LyHx",
                  "sentiment": "0",
                  "timestamp": 1492944619000
                  },
                  {
                  "tweet_id": "856097516361629696",
                  "created_date": "Sun Apr 23 17:48:48 WIB 2017",
                  "user_id": "2277291085",
                  "name": "Femi",
                  "screen_name": "Femmy_CR",
                  "image": "http://pbs.twimg.com/profile_images/855386747147919361/7sX6VnKl_normal.jpg",
                  "tweet_text": "RT @MosaToba: @kompascom Para Wajah Beringas Bersorban kontradiksi dg ajakan @aniesbaswedan utk bawa Pesan Damai.",
                  "sentiment": "0",
                  "timestamp": 1492944528000
                  }
              ]
          },

    
      "20170424":{
         "items1":[
                    {
                    "tweet_id": "856444244742950912",
                    "created_date": "Mon Apr 24 16:46:34 WIB 2017",
                    "user_id": "2453575590",
                    "name": "ابدل النحد",
                    "screen_name": "OdyiusJacare",
                    "image": "http://pbs.twimg.com/profile_images/855445737693298688/KzjrV8VD_normal.jpg",
                    "tweet_text": "RT @SuradjiHosni: @kompascom SBY yg menjadikan WaPres disebut ZALIM.\nJangan heran kelak giliran @jokowi  yg disebut ZALIM.",
                    "sentiment": "0",
                    "timestamp": 1493027194000
                    },
                    {
                    "tweet_id": "856443849694040064",
                    "created_date": "Mon Apr 24 16:45:00 WIB 2017",
                    "user_id": "462670954",
                    "name": "LhoMuslimKokEmosian?",
                    "screen_name": "UcengPilek",
                    "image": "http://pbs.twimg.com/profile_images/855066007978754052/y1vzJ8VG_normal.jpg",
                    "tweet_text": "RT @SuradjiHosni: @kompascom SBY yg menjadikan WaPres disebut ZALIM.\nJangan heran kelak giliran @jokowi  yg disebut ZALIM.",
                    "sentiment": "0",
                    "timestamp": 1493027100000
                    },
                    {
                    "tweet_id": "856443195646820353",
                    "created_date": "Mon Apr 24 16:42:24 WIB 2017",
                    "user_id": "1517748600",
                    "name": "#vicar_dust ✌",
                    "screen_name": "Kardusnyiromlah",
                    "image": "http://pbs.twimg.com/profile_images/799245740673372161/PR6h1kpY_normal.jpg",
                    "tweet_text": "Bagaimana  ini pak.....@basuki_btp @WagubDKI @boyrafliamar @DivHumasPolri @DKIJakarta @Dishubtrans_DKI @kompascom… https://t.co/PeWxNskoNo",
                    "sentiment": "0",
                    "timestamp": 1493026944000
                    },
                    {
                    "tweet_id": "856440791937335297",
                    "created_date": "Mon Apr 24 16:32:51 WIB 2017",
                    "user_id": "758679576499191808",
                    "name": "mashadi",
                    "screen_name": "mashadi421",
                    "image": "http://pbs.twimg.com/profile_images/854688420672684034/mqM_SLGa_normal.jpg",
                    "tweet_text": "@kompascom Janji yg umroh ada juga yg ke Roma dan Yerusalem ini perintah big boss @PartaiSocmed buzzernya kebagian… https://t.co/AmFtww9zP8",
                    "sentiment": "0",
                    "timestamp": 1493026371000
                    },

               ]
      },
      

      "20170425": {
         "items1":[
                   {
                    "tweet_id": "856846529843351552",
                    "created_date": "Tue Apr 25 19:25:06 WIB 2017",
                    "user_id": "619035146",
                    "name": "Cindy Chandra",
                    "screen_name": "gcindych",
                    "image": "http://pbs.twimg.com/profile_images/743670048527548420/-egekhf8_normal.jpg",
                    "tweet_text": "RT @kompascom: Banjir Karangan Bunga untuk Ahok dan Djarot di Balai Kota DKI. https://t.co/9tzzH2N4EH",
                    "sentiment": "0",
                    "timestamp": 1493123106000
                    },
                    {
                    "tweet_id": "856823254681493504",
                    "created_date": "Tue Apr 25 17:52:37 WIB 2017",
                    "user_id": "4398387972",
                    "name": "Michael 76N",
                    "screen_name": "michaellaen84",
                    "image": "http://pbs.twimg.com/profile_images/778037062867968000/THYY4udi_normal.jpg",
                    "tweet_text": "@ZaangGo @kompascom nyari simpati agar dipilih? pilkada udah selese cuy... nyari simpati agar apa?",
                    "sentiment": "-1",
                    "timestamp": 1493117557000
                    },
                    {
                    "tweet_id": "856816580608393216",
                    "created_date": "Tue Apr 25 17:26:06 WIB 2017",
                    "user_id": "871412892",
                    "name": "firmansyah adi",
                    "screen_name": "PHNCX",
                    "image": "http://pbs.twimg.com/profile_images/775075136923774977/I2DNnMib_normal.jpg",
                    "tweet_text": "RT @kompascom: \"Terima Kasih, Pak Jokowi!\". https://t.co/3i1OAZGQYF",
                    "sentiment": "0",
                    "timestamp": 1493115966000
                    },
                    {
                    "tweet_id": "856816401834815488",
                    "created_date": "Tue Apr 25 17:25:23 WIB 2017",
                    "user_id": "2376944491",
                    "name": "Smart Santa",
                    "screen_name": "SmartSanta",
                    "image": "http://pbs.twimg.com/profile_images/605053791889686528/XT0dbCZN_normal.jpg",
                    "tweet_text": "@jakpost @thejakartaglobe @kompascom @jawapos @tempodotco @detikcom @Metro_TV @KickAndyShow @OfficialRCTI @SCTV_… https://t.co/OiYLdLILx0",
                    "sentiment": "0",
                    "timestamp": 1493115923000
                    },
      ],
      },
      
    };
  

  constructor() { 
    this.options = {
          barBackground: '#656a6f',
          gridBackground: '#e1e1e1',
          barWidth: '5',
          gridWidth: '2'
        };
  }

  ngOnInit() {

    this.splitDays1 = this.tgl1.split(',');
    this.splitDays2 = this.tgl2.split(',');
    this.splitDays3 = this.tgl3.split(',');

    this.hari1 = this.splitDays1;
    this.hari2 = this.splitDays2;
    this.hari3 = this.splitDays3;

    this.itemsDay1 = this.timeline["20170423"].items1;
    this.itemsDay2 = this.timeline["20170424"].items1;
    this.itemsDay3 = this.timeline["20170425"].items1;
    console.log(this.timeline["20170425"].items1);
    console.log(this.tgl2);
    console.log(this.splitDays1);
    console.log(this.hari1);
    
      this.dates = [];
      this.date = new Date();
      this.loadTimeline();
  }

  prevDay(event: any) {
    this.date = this.prevDate;
    this.loadTimeline();
    // console.log(this.date1 + this.date2 + this.date3)
  }

  nextDay(event: any) {
    this.date = this.nextDate;
    this.loadTimeline();
  }


  loadTimeline() {
    this.dates = [];
    this.prevDate = new Date(this.date);
    this.prevDate.setDate(this.date.getDate() - 1);
    this.nextDate = new Date(this.date);
    this.nextDate.setDate(this.date.getDate() + 1);

    this.items = [];
    for (var i = 2; i >= 0; i--) {
      var tempd = new Date(this.date);
      tempd.setDate(tempd.getDate() - i);
      var day = tempd.getDate();
      var monthIndex = tempd.getMonth();
      var year = tempd.getFullYear();
      if (i == 2) {
        this.date1 = year + ('0' + (monthIndex + 1)).slice(-2) + ('0' + day).slice(-2);
      }
      else if (i == 1) {
        this.date2 = year + ('0' + (monthIndex + 1)).slice(-2) + ('0' + day).slice(-2);
      }
      else {
        this.date3 = year + ('0' + (monthIndex + 1)).slice(-2) + ('0' + day).slice(-2);
      }
      this.items.push({ id: year + ('0' + (monthIndex + 1)).slice(-2) + ('0' + day).slice(-2), text: GlobalVariable.days[(tempd.getDay() == 0 ? 7 : tempd.getDay())] + ', ' + day + ' ' + GlobalVariable.months[monthIndex] });
      setTimeout(function () { }, 5000);
    }
    this.days = this.date1 + ',' + this.date2 + ',' + this.date3;

  }
}
