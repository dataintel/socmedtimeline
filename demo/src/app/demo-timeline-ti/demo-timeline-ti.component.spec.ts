import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoTimelineTiComponent } from './demo-timeline-ti.component';

describe('DemoTimelineTiComponent', () => {
  let component: DemoTimelineTiComponent;
  let fixture: ComponentFixture<DemoTimelineTiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemoTimelineTiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemoTimelineTiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
