import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { SlimScrollModule } from 'ng2-slimscroll';
import { AppComponent } from './app.component';
import { TimelineTitleModule } from '../../../index';
import { DemoTimelineTiComponent } from './demo-timeline-ti/demo-timeline-ti.component';

@NgModule({
  declarations: [
    AppComponent,
    DemoTimelineTiComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SlimScrollModule,
    TimelineTitleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
