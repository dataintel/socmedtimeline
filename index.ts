import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetSocmedTimelineComponent } from './src/timeline-title.component';
import { SlimScrollModule } from 'ng2-slimscroll';

export * from './src/timeline-title.component';

@NgModule({
  imports: [
    CommonModule,
    SlimScrollModule
  ],
  declarations: [
    BaseWidgetSocmedTimelineComponent,
   
  ],
  exports: [
    BaseWidgetSocmedTimelineComponent,
  
  ]
})
export class TimelineSocmedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TimelineSocmedModule,
      providers: []
    };
  }
}
